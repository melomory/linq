﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Shop.Dto;
using Shop.Enums;
using Shop.Models;

namespace Shop
{
	public class ProductService
	{
		private readonly ShopContext _dbContext;

		private IList<Product> Products => _dbContext.Products.Include(p => p.Category).ToList();

		public ProductService(ShopContext dbContext)
		{
			_dbContext = dbContext;
		}

		/// <summary>
		/// Сортирует товары по цене и возвращает отсортированный список
		/// </summary>
		/// <param name="sortOrder">Порядок сортировки</param>
		public IEnumerable<Product> SortByPrice(SortOrder sortOrder)
		{
			return sortOrder switch
            {
                SortOrder.Ascending => Products.OrderBy(product => product.Price),
                SortOrder.Descending => Products.OrderByDescending(product => product.Price),
                _ => throw new NotImplementedException()
            };
        }

        /// <summary>
        /// Возвращает товары, название которых начинается на <see cref="name"/>
        /// </summary>
        /// <param name="name">Фильтр - строка, с которой начинается название товара</param>
        public IEnumerable<Product> FilterByNameStart(string name)
		{
			return Products.Where(product => product.Name.StartsWith(name))
				           .Select(product => product);
		}

		/// <summary>
		/// Группирует товары по производителю
		/// </summary>
		public IDictionary<string, List<Product>> GroupByVendor()
		{
			return Products.GroupBy(product => product.Vendor).ToDictionary(vendor => vendor.Key, products => products.ToList());
		}

		/// <summary>
		/// Возвращает список самых дорогих товаров (самые дорогие - товары с наибольшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheMostExpensiveProducts()
		{
			decimal maxPrice = Products.Max(product => product.Price);
			return Products.Where(product => maxPrice == product.Price);
		}

		/// <summary>
		/// Возвращает список самых дешевых товаров (самые дешевые - товары с наименьшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheCheapestProducts()
		{
			decimal minPrice = Products.Min(product => product.Price);
			return Products.Where(product => minPrice == product.Price);
		}

		/// <summary>
		/// Возвращает среднюю цену среди всех товаров
		/// </summary>
		public decimal GetAverageProductPrice()
		{
			return Products.Average(product => product.Price);
		}

		/// <summary>
		/// Возвращает среднюю цену товаров в указанной категории
		/// </summary>
		/// <param name="categoryId">Идентификатор категории</param>
		public decimal GetAverageProductPriceInCategory(int categoryId)
		{
			return Products.Where(product => product.CategoryId == categoryId)
				           .Average(product => product.Price);
		}

		/// <summary>
		/// Возвращает список продуктов с актуальной ценой (после применения скидки)
		/// </summary>
		public IDictionary<Product, decimal> GetProductsWithActualPrice()
		{
			return Products.Where(product => product.Discount >= 0)
				           .ToDictionary(product => product, product => product.Price * (100 - product.Discount) / 100);
		}

		/// <summary>
		/// Возвращает список продуктов, сгруппированный по производителю, а внутри - по названию категории.
		/// Продукты внутри последней группы отсортированы в порядке убывания цены
		/// </summary>
		public IList<VendorProductsDto> GetGroupedByVendorAndCategoryProducts()
		{
			return Products
				.GroupBy(product => product.Vendor)
				.Select(groupedByVendor => new VendorProductsDto()
				{ Vendor = groupedByVendor.Key, 
				  CategoryProducts = groupedByVendor
					.GroupBy(product => product.Category)
					.ToDictionary
					(
					  category => category.Key.Name, 
					  groupedByCategoryProducts => groupedByCategoryProducts.Key.Products
						.OrderByDescending(groupedByCategoryProduct => groupedByCategoryProduct.Price)
						.ToList()
					) 
				}).ToList();
		}

		/// <summary>
		/// Обновляет скидку на товары, которые остались на складе в количестве 1 шт,
		/// и возвращает список обновленных товаров
		/// </summary>
		/// <param name="newDiscount">Новый процент скидки</param>
		public IEnumerable<Product> UpdateDiscountIfUnitsInStockEquals1AndGetUpdatedProducts(int newDiscount)
		{
			return Products.Where(product => product.UnitsInStock == 1)
				           .Select(product => { product.Discount = newDiscount; return product; });
		}
	}
}
